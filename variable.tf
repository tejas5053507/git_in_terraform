variable "image_id" {
    default = "ami-0440d3b780d96b29d"
}
variable "instance_type" {
    default = "t2.micro"
}
variable "key_pair" {
    default = "universal_key"
}
variable "git" {
    default = "git_lauch"
}
variable "env" {
    default = "dev"
}
variable "security_group_id" {
    default = ["sg-0b2ba3f8a94da51b1"]
}

