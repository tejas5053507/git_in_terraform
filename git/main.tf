resource "github_repository" "tejas" {
  name        = "tejas"
  description = "My awesome codebase"

  visibility = "public"

  template {
    owner                = "github"
    repository           = "terraform-template-module"
    include_all_branches = true
  }
}
resource "github_branch" "development" {
  repository = "tejas"
  branch     = "development"
}


  user_data = <<-EOF
             #!/bin/bash
             yum install git -y
             mkdir -p /root/tej
             cd /root/tej
             git init
             echo ""<h1>hello friends, welcome to the home page</h1>" > /var/www/html/index.html
             git add index.html
             git commit -m "First commit"
             git config --global user.email tsp192002@gmail.com
             git config --global  user.name tejas192002
             git checkout -b dev
             git merge master
             git checkout master
             git remote add tejas https://github.com/tejas192002/tej.git
             git push -f tej master
             EOF