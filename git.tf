provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "git-task" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name      = var.key_pair
   vpc_security_group_ids = var.security_group_id

  provisioner "remote-exec" {
    inline = [
      "sudo yum install git -y",
      "mkdir tej", 
      "cd tej && git init",
      "echo 'welcome tejas' > index.html",
      "git add index.html", 
      "git commit -m 'Initial commit'", 
      "git branch dev"
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("./universal_key.pem")
      host        = self.public_ip
    }
  }
}
